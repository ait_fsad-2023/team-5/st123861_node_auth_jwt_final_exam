const mongoose = require('mongoose');
const { isEmail } = require('validator');

validate: [isEmail, 'Please enter a valid email']

const userSchema = new mongoose.Schema({
    email: {
        type:String,
        required: true,
        unique: true,
        lowercase: true,
    },
    password: {
        type: String,
        required: true,
        minlenght: 6,
    }
});

// duplicate email error
// if (err.code === 11000) {
//     errors.email = 'that email is already registered';
//     return errors;
// } 

userSchema.pre('save', async function(next) {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
    next();
});

const User = mongoose.model('user', userSchema);
module.exports = User;