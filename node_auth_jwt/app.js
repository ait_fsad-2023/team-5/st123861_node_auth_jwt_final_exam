const express = require('express');
const mongoose = require('mongoose');
const authRouth = require('./routes/authRouth.js');
const cookieParser = require('cookie-parser')

const app = express();

// middleware
app.use(express.static('public'));

app.use(authRouth);
app.use(cookieParser);

// view engine
app.set('view engine', 'ejs');

// database connection
const dbURI = 'mongodb+srv://st123861:Viseszri1991Best@cluster0.krss1bo.mongodb.net/NodeAuth';
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true })
  .then((result) => app.listen(3000))
  .catch((err) => console.log(err));

// routes
app.get('/', (req, res) => res.render('home'));
app.get('/smoothies', (req, res) => res.render('smoothies'));